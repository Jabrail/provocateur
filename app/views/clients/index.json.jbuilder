json.array!(@clients) do |client|
  json.extract! client, :id, :name, :last_name, :login, :pass, :token
  json.url client_url(client, format: :json)
end
