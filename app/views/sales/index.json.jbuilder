json.array!(@sales) do |sale|
  json.extract! sale, :id, :name, :sale, :start_day, :finish_day
  json.url sale_url(sale, format: :json)
end
