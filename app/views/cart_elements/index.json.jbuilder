json.array!(@cart_elements) do |cart_element|
  json.extract! cart_element, :id, :count, :size, :color
  json.url cart_element_url(cart_element, format: :json)
end
