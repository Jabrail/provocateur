json.array!(@orders) do |order|
  json.extract! order, :id, :c_id, :p_id, :count
  json.url order_url(order, format: :json)
end
