json.items(@categories) do |category|
  json.extract! category, :id, :name
  json.avatar  request.protocol + request.host_with_port + category.avatar.url
  json.items_count category.products.where(:show => 1).count
  json.url category_url(category, format: :json)
end

if @client != nil
  json.token @client.token
end