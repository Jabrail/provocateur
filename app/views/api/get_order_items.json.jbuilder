json.elements(@cart_elements) do |element|
  json.extract! element, :id, :count, :color, :size
  json.product element.product, :id, :name, :brend, :images, :sizeTable, :description, :price, :sales
  json.avatar request.protocol + request.host_with_port + element.product.avatar.url
end
