json.products(@products) do |product|
  json.extract! product, :id, :name, :price, :brend

  json.avatar request.protocol + request.host_with_port + product.avatar.url

  sale = 0
  product.sales.each do |sl|
    sale += sl.sale
  end
  json.sale = sale
  json.url category_url(product, format: :json)

end