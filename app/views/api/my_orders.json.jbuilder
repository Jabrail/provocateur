json.orders(@orders) do |order|
  json.extract! order, :id, :status
end
