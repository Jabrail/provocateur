json.items(@blogs) do |blog|
  json.extract! blog, :id, :desc
  json.avatar  request.protocol + request.host_with_port + blog.avatar.url
end
