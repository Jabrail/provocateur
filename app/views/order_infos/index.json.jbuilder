json.array!(@order_infos) do |order_info|
  json.extract! order_info, :id, :c_id, :integer, :type, :name, :last_name, :middle_name, :country, :city, :region, :index, :street, :house, :parch, :flat, :phone, :commit
  json.url order_info_url(order_info, format: :json)
end
