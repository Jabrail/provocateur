json.array!(@products) do |product|
  json.extract! product, :id, :name, :size, :sizeTable, :color, :description, :price, :sale
  json.url product_url(product, format: :json)
end
