// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require jquery.Jcrop
//= require_tree .

function addCat(p_id) {
    var c_id = $('#prod_cat_id').val()
    $('#prod_cat_id option[value='+c_id+']').remove();
    $.ajax({
        method: "GET",
        url: "/products/"+p_id+"/add_category",
        data: { c_id: c_id}
    })
        .done(function( data ) {
            var node =    '<li class="list-group-item" style="width: 200px; float: left" id="cat_'+c_id+'">' +
                '<span class=" fa fa-remove " aria-hidden="true" style="float: right;" onclick="removeCat('+c_id+','+p_id+')"></span>' +
                data.name +
                '</li>';
            $('#categories').append(node)
            $('#myModal').modal('hide')
        });
}

function addSale(p_id) {
    var s_id = $('#prod_sale_id').val()
    $('#prod_sale_id option[value='+s_id+']').remove();
    $.ajax({
        method: "GET",
        url: "/products/"+p_id+"/add_sale",
        data: { s_id: s_id}
    })
        .done(function( data ) {
            var node =    '<li class="list-group-item" style="width: 200px; float: left" id="sale_'+s_id+'">' +
                '<span class=" fa fa-remove " aria-hidden="true" style="float: right;" onclick="removeSale('+s_id+','+p_id+')"></span>' +
                data.name +
                '</li>';
            $('#sales').append(node)
            $('#SaleModal').modal('hide')
        });
}

function removeCat(c_id, p_id) {

    $.ajax({
        method: "GET",
        url: "/products/"+p_id+"/remove_cat",
        data: { c_id: c_id}
    })
        .done(function( data ) {

            $('#prod_cat_id').append('<option value="'+c_id+'">'+$('#cat_'+c_id).text()+'</option>')
            $('#cat_'+c_id).remove()


        });
}

function removeSale(s_id, p_id) {

    $.ajax({
        method: "GET",
        url: "/products/"+p_id+"/remove_sale",
        data: { s_id: s_id}
    })
        .done(function( data ) {

            $('#prod_sale_id').append('<option value="'+s_id+'">'+$('#sale_'+s_id).text()+'</option>')
            $('#sale_'+s_id).remove()


        });
}

$('#new_image').on('ajax:error', function(event, xhr, status, error) {
    // insert the failure message inside the "#account_settings" element
    $(this).append(xhr.responseText)
});