class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, only: [ :create, :update ]
  before_filter :authenticate_user!
  skip_before_filter :authenticate_user!, only: [:create]
  require 'httpclient'
  require 'uri'
  require 'digest/md5'

  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.all
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
  end

  # GET /clients/new
  def new
    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create

    @client = Client.find_by_login(client_params[:login])

    if @client == nil

      @client = Client.find_by token: params[:token]
      @client.name = client_params[:name]
      @client.last_name = client_params[:last_name]
      @client.login = client_params[:login]


    pass = rand(9).to_s + rand(9).to_s + rand(9).to_s + rand(9).to_s

    @client.login = client_params[:login].strip
    @client.pass = Digest::MD5.hexdigest(pass)
    pass = URI.encode('\n Ваш пароль: '+pass+' \n Спасибо за регистрацию')

    else

      if @client.token != params[:token]

        @tmp_client = Client.find_by_token(params[:token])

        cart = @client.cart

        @tmp_client.cart.cart_elements.each do |element|

          newElement = cart.cart_elements.new
          newElement.count = element.count
          newElement.product = element.product
          newElement.color = element.color
          newElement.size = element.size
          newElement.save
          element.destroy
        end

        @tmp_client.destroy

      end
      @client.pass = Digest::MD5.hexdigest(pass)
      pass = URI.encode('\n Ваш пароль: '+pass+' \n Спасибо за регистрацию')

    end
    url = 'http://sms.ru/sms/send?api_id=8d9c6d58-bac9-8484-358f-ea2ff4686791&from=Provocateur&to='+URI.encode(@client.login)+'&text=' + pass

    respond_to do |format|
      if @client.save

        clnt = HTTPClient.new
        puts clnt.get_content(url)


        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { }
      else
        format.html { render :new }
        format.json {  }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update

    pass = rand(9).to_s + rand(9).to_s + rand(9).to_s + rand(9).to_s

    @client.pass = pass
    pass = URI.encode('\n Ваш пароль: '+pass)

    respond_to do |format|
      if @client.update(client_params)

        clnt = HTTPClient.new
        puts clnt.get_content('http://sms.ru/sms/send?api_id=8d9c6d58-bac9-8484-358f-ea2ff4686791&to='+@client.login+'&text=' + pass)


        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:name, :last_name, :login, :pass, :token)
    end
end
