class CartElementsController < ApplicationController
  before_action :set_cart_element, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
 # skip_before_filter :authenticate_user!, only: [:create]

  # GET /cart_elements
  # GET /cart_elements.json
  def index
    @cart_elements = CartElement.all
  end

  # GET /cart_elements/1
  # GET /cart_elements/1.json
  def show
  end

  # GET /cart_elements/new
  def new
    @cart_element = CartElement.new
  end

  # GET /cart_elements/1/edit
  def edit
  end

  # POST /cart_elements
  # POST /cart_elements.json
  def create
    @cart_element = CartElement.new(cart_element_params)

    respond_to do |format|
      if @cart_element.save
        format.html { redirect_to @cart_element, notice: 'Cart element was successfully created.' }
        format.json { render :show, status: :created, location: @cart_element }
      else
        format.html { render :new }
        format.json { render json: @cart_element.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cart_elements/1
  # PATCH/PUT /cart_elements/1.json
  def update
    respond_to do |format|
      if @cart_element.update(cart_element_params)
        format.html { redirect_to @cart_element, notice: 'Cart element was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart_element }
      else
        format.html { render :edit }
        format.json { render json: @cart_element.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cart_elements/1
  # DELETE /cart_elements/1.json
  def destroy
    @cart_element.destroy
    respond_to do |format|
      format.html { redirect_to cart_elements_url, notice: 'Cart element was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_element
      @cart_element = CartElement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_element_params
      params.require(:cart_element).permit(:count, :size, :color, :status)
    end
end
