class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_action :verify_authenticity_token, only: [ :change_shows ]

  # skip_before_filter :authenticate_user!, only: [:index]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show

  @image = Image.new

  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end


  def add_category

    if  Product.find(params[:id]).categories.exists?(params[:c_id])
      render :json  => { :name => "error" }
else

      newRel = ProdCat.new
      newRel.product = Product.find(params[:id])
      newRel.category = Category.find(params[:c_id])
      newRel.save
      render :json  => { :name => newRel.category.name }
end



  end

  def remove_cat
    Product.find(params[:id]).categories.delete(params[:c_id])

   render :json  => { :name => Product.find(params[:id]).categories }

  end

  def add_sale

    if  Product.find(params[:id]).sales.exists?(params[:s_id])
      render :json  => { :name => "error" }
else

      newRel = ProdSale.new
      newRel.product = Product.find(params[:id])
      newRel.sale = Sale.find(params[:s_id])
      newRel.save
      render :json  => { :name => newRel.sale.name }
end



  end

  def remove_sale
    Product.find(params[:id]).sales.delete(params[:s_id])

   render :json  => { :name => Product.find(params[:id]).sales }

  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

       if @product.save
        flash[:notice] = 'User was successfully created.'
        if params[:product][:avatar].blank?
          redirect_to(@product)
        else
          puts @product.name
          render :action => 'edit'
        end
      else
        render :action => 'new'
      end
   end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update


    if @product.update_attributes(product_params)
      if params[:product][:avatar].blank?
        flash[:notice] = "Successfully updated user."
        redirect_to @product
      else
        render :action => "edit"
      end
    else
      render :action => 'edit'
    end

  end

  def change_show

    @product = Product.find(params[:id])

    @product.show = params[:show]

    @product.save

    render nil

  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :size, :sizeTable, :color, :description, :price, :sale, :avatar, :show, :brend, :count, :x1, :y1, :width, :height)
    end
end
