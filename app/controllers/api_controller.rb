class ApiController < ApplicationController
  before_action :set_client, only: [:add_cart, :add_items , :add_item, :get_cart, :pay_success, :my_orders, :get_order_items, :add_item_count, :remove_item_count, :remove_item]

  skip_before_action :verify_authenticity_token

  require 'digest/md5'
  require 'json'

  def categories

    @categories = Category.all

    respond_to do |format|

      if params[:token] == "nil"

        @client = Client.new
        @client.token = loop do
          token = SecureRandom.urlsafe_base64
          break token unless Client.exists?(token: token)
        end

        @client.cart = Cart.create()
        @client.save

      else

        if params[:token] != nil

          @client = Client.find_by_token(params[:token])

        else
          @client = nil
        end

      end

      respond = {:items => @categories}
      format.html { render  hrml: @categories }
      format.json {  }

    end

  end

  def products

    @products = Category.find(params[:id]).products.where(:show => 1)

    respond_to do |format|
      format.json   {}
    end
  end

  def product

    @product = Product.find(params[:id])
    respond_to do |format|
      format.json   {}
    end
  end

  def authorization

    @client = Client.find_by_login(params[:login].strip)

    respond_to do |format|

      if @client

        if @client.pass ==  Digest::MD5.hexdigest(params[:pass])

          format.json   {}

        else
          format.json  { render json:  { status: 101 } }
        end

      else
        format.json  { render json:  { status: 101 } }
      end


    end

  end

  def add_cart

    if @client
      @newRel = UserCart.new
      @newRel.client = @client
      cart = Cart.new
      cart.status = 0
      cart.save
      @newRel.cart = cart
      @newRel.save
    end

  end

  def add_item

    if @client

      cart = @client.cart
      element = cart.cart_elements.new
      element.count = params[:count]
      element.product = Product.find(params[:p_id])
      element.color = params[:color]
      element.size = params[:size]
      element.status = 0
      element.save

    end

  end

  def add_items

    if @client

      cart =   Cart.find(params[:c_id])
      items = ActiveSupport::JSON.decode(params[:items])
      puts items.length
      items.each do |item|
        element = cart.cart_elements.new
        element.count = item["count"]
        element.product = Product.find(item["p_id"])
        element.color = item["color"]
        element.size = item["size"]
        element.save
      end

    end

  end

  def pay_success

    respond_to do |format|

      if @client != nil

        order = @client.orders.new

        order.type = params[:type]
        order.name = params[:name]
        order.last_name = params[:last_name]
        order.middle_name = params[:middle_name]
        order.country = params[:country]
        order.region = params[:region]
        order.index = params[:index]
        order.street = params[:street]
        order.house = params[:house]
        order.commit = params[:comment]
        order.phone = @client.login
        order.status = 0


        if order.save

          @client.cart.cart_elements.each do |element|
            if element.status == 0
              element.status = 1
              element.order = order
              element.save
            end
          end
          format.json { render json: {status: 100} }
        else
          format.json { render json: {status: 101} }
        end

      else

        format.json { render json: {status: 105} }

      end

    end

=begin

    hash = ActiveSupport::HashWithIndifferentAccess.new(JSON.parse(params[:details].to_json, :quirks_mode => true))
    puts hash
   # hash = ActiveSupport::HashWithIndifferentAccess.new(params[:details])
    hash = hash.dup
    puts("99999999999999999999999")
    puts(hash[0]["response"])
=end

  end

  def get_order_items

    if @client != nil
      order = Order.find(params[:order_id])
      @cart_elements = CartElement.where(:order => order)
    end

  end

  def blog

    @blogs = Blog.where(:status => 1)

    respond_to do |format|
      format.json   {}
    end

  end


  def get_cart

    if @client
      @cart_elements = @client.cart.cart_elements.where(:status => 0)
    else
      render json: {status: 105}
    end

  end

  def my_orders
    @orders = @client.orders
    if @clients

      @orders = @client.orders

    end
  end

  def add_item_count
    respond_to do |format|
      if @client

        cart_element = CartElement.find(params[:item_id])
        cart_element.count = cart_element.count + 1
        cart_element.save
        format.json { render json: {status: 100} }
      else
        format.json { render json: {status: 105} }
      end


    end

  end

  def remove_item_count
    respond_to do |format|
      if @client

        cart_element = CartElement.find(params[:item_id])
        if cart_element.count > 0
          cart_element.count = cart_element.count - 1
          cart_element.save
          format.json { render json: {status: 100} }

        else

          format.json { render json: {status: 106} }

        end

      else
        format.json { render json: {status: 105} }
      end
    end
  end

  def search

    prod = Product.arel_table

    @products = Product.where(prod[:name].matches("%#{params[:string]}%"))

    respond_to do |format|
      format.json   {}
    end

  end

  def remove_item

    respond_to do |format|
      if @client

        cart_element = CartElement.find(params[:item_id])

        cart_element.destroy

        format.json { render json: {status: 100} }

      else
        format.json { render json: {status: 105} }
      end
    end

  end

  def set_client

    @client = Client.find_by_token(params[:token])

  end

end
