class Sale < ActiveRecord::Base
  has_many :prod_sale
  has_many :products, through: :prod_sale
end
