class Cart < ActiveRecord::Base

  has_many :user_cart
  has_many :clients, through: :user_cart

  has_many :cart_order
  has_many :orders, through: :cart_order

  has_many :cart_elements

end
