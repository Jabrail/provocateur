class Order < ActiveRecord::Base

  has_many :cart_order
  has_many :carts, through: :cart_order

  belongs_to :client

end
