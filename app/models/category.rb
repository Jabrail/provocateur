class Category < ActiveRecord::Base


  require 'RMagick'

  has_attached_file :avatar,
                    :styles => {
                        :thumb => ["100x100", :jpg],
                        :pagesize => ["500x400", :jpg],
                    },
                    :default_style => :pagesize


  attr_accessor :x1, :y1, :width, :height

  def update_attributes(att)

    scaled_img = Magick::ImageList.new(self.avatar.path)
    orig_img = Magick::ImageList.new(self.avatar.path(:original))
    scale = orig_img.columns.to_f / scaled_img.columns

    args = [ att[:x1], att[:y1], att[:width], att[:height] ]
    args = args.collect { |a| a.to_i * scale }

    orig_img.crop!(*args)
    orig_img.write(self.avatar.path(:original))

    self.avatar.reprocess!
    self.save

    super(att)
  end

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  has_many :prod_cats
  has_many :products, through: :prod_cats
end
