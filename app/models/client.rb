class Client < ActiveRecord::Base

  belongs_to :cart
  has_many :orders

end
