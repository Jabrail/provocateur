Rails.application.routes.draw do

#  resources :users

  devise_for :users

  get 'admin/index', as: 'user_root'

#  get 'admin/index'

  get 'admin' => 'admin#index'

  resources :blogs

  resources :cart_elements

  resources :order_infos

  resources :orders

  resources :clients

  get 'home/index'

  resources :images

  resources :sales

  resources :products

  get 'products/:id/add_category' => 'products#add_category', as: :add_category
  get 'products/:id/remove_cat' => 'products#remove_cat', as: :remove_cat
  get 'products/:id/add_sale' => 'products#add_sale', as: :add_sale
  get 'products/:id/remove_sale' => 'products#remove_sale', as: :remove_sale
  post 'products/:id/change_show' => 'products#change_show', as: :change_show


  get 'api/products/:id' => 'api#products'
  get 'api/product/:id' => 'api#product'
  get 'api/categories' => 'api#categories'
  get 'api/get_cart' => 'api#get_cart'
  get 'api/blog' => 'api#blog'
  get 'api/my_orders' => 'api#my_orders'
  get 'api/get_order_items' => 'api#get_order_items'

  post 'api/auth' => 'api#authorization'
  post 'api/add_cart' => 'api#add_cart'
  post 'api/add_item' => 'api#add_item'
  post 'api/add_items' => 'api#add_items'
  post 'api/pay_success' => 'api#pay_success'
  post 'api/add_item_count' => 'api#add_item_count'
  post 'api/remove_item_count' => 'api#remove_item_count'
  post 'api/remove_item' => 'api#remove_item'
  post 'api/search' => 'api#search'



  resources :categories

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
