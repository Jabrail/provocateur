class CreateUserCarts < ActiveRecord::Migration
  def change
    create_table :user_carts do |t|

        t.belongs_to :client, index: true
        t.belongs_to :cart, index: true

        t.timestamps
    end
  end
end
