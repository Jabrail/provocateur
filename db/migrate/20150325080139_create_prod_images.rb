class CreateProdImages < ActiveRecord::Migration
  def change
    create_table :prod_images do |t|

      t.belongs_to :image, index: true
      t.belongs_to :product, index: true

      t.timestamps
    end
  end
end
