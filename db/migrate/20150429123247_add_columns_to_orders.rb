class AddColumnsToOrders < ActiveRecord::Migration
  def change

    add_column :orders, :type, :integer
    add_column :orders, :name, :string
    add_column :orders, :last_name, :string
    add_column :orders, :middle_name, :string
    add_column :orders, :country, :string
    add_column :orders, :city, :string
    add_column :orders, :region, :string
    add_column :orders, :index, :string
    add_column :orders, :street, :string
    add_column :orders, :house, :string
    add_column :orders, :parch, :string
    add_column :orders, :flat, :string
    add_column :orders, :phone, :string
    add_column :orders, :commit, :string
    add_column :orders, :client_id, :integer


    add_index :orders, :client_id

  end
end
