class CreateCartElements < ActiveRecord::Migration
  def change
    create_table :cart_elements do |t|
      t.integer :count
      t.string  :size
      t.string  :color
      t.integer :status

      t.belongs_to :cart, index: true
      t.belongs_to :product, index: true

      t.timestamps
    end
  end
end
