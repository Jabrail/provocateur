class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :last_name
      t.string :login
      t.string :pass
      t.string :token
      t.belongs_to :cart, index: true


      t.timestamps
    end
  end
end
