class AddColumnDeviceIdToClient < ActiveRecord::Migration
  def change
    add_column :clients, :deviceId, :string
  end
end
