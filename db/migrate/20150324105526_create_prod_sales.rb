class CreateProdSales < ActiveRecord::Migration
  def change
    create_table :prod_sales do |t|

      t.belongs_to :sale, index: true
      t.belongs_to :product, index: true

      t.timestamps
    end
  end
end
