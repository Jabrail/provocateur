class CreateCartOrders < ActiveRecord::Migration
  def change
    create_table :cart_orders do |t|


      t.belongs_to :order, index: true
      t.belongs_to :cart, index: true

      t.timestamps
    end
  end
end
