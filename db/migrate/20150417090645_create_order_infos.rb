class CreateOrderInfos < ActiveRecord::Migration
  def change
    create_table :order_infos do |t|
      t.string :c_id
      t.string :integer
      t.integer :type
      t.string :name
      t.string :last_name
      t.string :middle_name
      t.string :country
      t.string :city
      t.string :region
      t.string :index
      t.string :street
      t.string :house
      t.string :parch
      t.string :flat
      t.string :phone
      t.string :commit

      t.timestamps
    end
  end
end
