class AddColumnToCarElements < ActiveRecord::Migration
  def change
    add_column :cart_elements, :order_id, :integer
    add_index :cart_elements, :order_id
  end
end
