class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :size
      t.integer :sizeTable
      t.string :color
      t.text :description
      t.string :price
      t.integer :sale

      t.timestamps
    end
  end
end
