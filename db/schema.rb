# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150604094334) do

  create_table "blogs", force: true do |t|
    t.string   "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "status"
  end

  create_table "cart_elements", force: true do |t|
    t.integer  "count"
    t.string   "size"
    t.string   "color"
    t.integer  "status"
    t.integer  "cart_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  add_index "cart_elements", ["cart_id"], name: "index_cart_elements_on_cart_id"
  add_index "cart_elements", ["order_id"], name: "index_cart_elements_on_order_id"
  add_index "cart_elements", ["product_id"], name: "index_cart_elements_on_product_id"

  create_table "cart_orders", force: true do |t|
    t.integer  "order_id"
    t.integer  "cart_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cart_orders", ["cart_id"], name: "index_cart_orders_on_cart_id"
  add_index "cart_orders", ["order_id"], name: "index_cart_orders_on_order_id"

  create_table "carts", force: true do |t|
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "clients", force: true do |t|
    t.string   "name"
    t.string   "last_name"
    t.string   "login"
    t.string   "pass"
    t.string   "token"
    t.integer  "cart_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "deviceId"
  end

  add_index "clients", ["cart_id"], name: "index_clients_on_cart_id"

  create_table "images", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "order_infos", force: true do |t|
    t.string   "c_id"
    t.string   "integer"
    t.integer  "type"
    t.string   "name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "country"
    t.string   "city"
    t.string   "region"
    t.string   "index"
    t.string   "street"
    t.string   "house"
    t.string   "parch"
    t.string   "flat"
    t.string   "phone"
    t.string   "commit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.integer  "p_id"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type"
    t.string   "name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "country"
    t.string   "city"
    t.string   "region"
    t.string   "index"
    t.string   "street"
    t.string   "house"
    t.string   "parch"
    t.string   "flat"
    t.string   "phone"
    t.string   "commit"
    t.integer  "client_id"
    t.integer  "status"
  end

  add_index "orders", ["client_id"], name: "index_orders_on_client_id"

  create_table "prod_cats", force: true do |t|
    t.integer  "category_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "prod_cats", ["category_id"], name: "index_prod_cats_on_category_id"
  add_index "prod_cats", ["product_id"], name: "index_prod_cats_on_product_id"

  create_table "prod_images", force: true do |t|
    t.integer  "image_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "prod_images", ["image_id"], name: "index_prod_images_on_image_id"
  add_index "prod_images", ["product_id"], name: "index_prod_images_on_product_id"

  create_table "prod_sales", force: true do |t|
    t.integer  "sale_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "prod_sales", ["product_id"], name: "index_prod_sales_on_product_id"
  add_index "prod_sales", ["sale_id"], name: "index_prod_sales_on_sale_id"

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "size"
    t.integer  "sizeTable"
    t.string   "color"
    t.text     "description"
    t.string   "price"
    t.integer  "sale"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "show"
    t.string   "brend"
    t.integer  "count"
  end

  create_table "sales", force: true do |t|
    t.string   "name"
    t.integer  "sale"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_carts", force: true do |t|
    t.integer  "client_id"
    t.integer  "cart_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_carts", ["cart_id"], name: "index_user_carts_on_cart_id"
  add_index "user_carts", ["client_id"], name: "index_user_carts_on_client_id"

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "last_name"
    t.integer  "rule"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
