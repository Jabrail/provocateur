require 'test_helper'

class OrderInfosControllerTest < ActionController::TestCase
  setup do
    @order_info = order_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:order_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order_info" do
    assert_difference('OrderInfo.count') do
      post :create, order_info: { c_id: @order_info.c_id, city: @order_info.city, commit: @order_info.commit, country: @order_info.country, flat: @order_info.flat, house: @order_info.house, index: @order_info.index, integer: @order_info.integer, last_name: @order_info.last_name, middle_name: @order_info.middle_name, name: @order_info.name, parch: @order_info.parch, phone: @order_info.phone, region: @order_info.region, street: @order_info.street, type: @order_info.type }
    end

    assert_redirected_to order_info_path(assigns(:order_info))
  end

  test "should show order_info" do
    get :show, id: @order_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order_info
    assert_response :success
  end

  test "should update order_info" do
    patch :update, id: @order_info, order_info: { c_id: @order_info.c_id, city: @order_info.city, commit: @order_info.commit, country: @order_info.country, flat: @order_info.flat, house: @order_info.house, index: @order_info.index, integer: @order_info.integer, last_name: @order_info.last_name, middle_name: @order_info.middle_name, name: @order_info.name, parch: @order_info.parch, phone: @order_info.phone, region: @order_info.region, street: @order_info.street, type: @order_info.type }
    assert_redirected_to order_info_path(assigns(:order_info))
  end

  test "should destroy order_info" do
    assert_difference('OrderInfo.count', -1) do
      delete :destroy, id: @order_info
    end

    assert_redirected_to order_infos_path
  end
end
