require 'test_helper'

class CartElementsControllerTest < ActionController::TestCase
  setup do
    @cart_element = cart_elements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cart_elements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cart_element" do
    assert_difference('CartElement.count') do
      post :create, cart_element: { color: @cart_element.color, count: @cart_element.count, size: @cart_element.size }
    end

    assert_redirected_to cart_element_path(assigns(:cart_element))
  end

  test "should show cart_element" do
    get :show, id: @cart_element
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cart_element
    assert_response :success
  end

  test "should update cart_element" do
    patch :update, id: @cart_element, cart_element: { color: @cart_element.color, count: @cart_element.count, size: @cart_element.size }
    assert_redirected_to cart_element_path(assigns(:cart_element))
  end

  test "should destroy cart_element" do
    assert_difference('CartElement.count', -1) do
      delete :destroy, id: @cart_element
    end

    assert_redirected_to cart_elements_path
  end
end
